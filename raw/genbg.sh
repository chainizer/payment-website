#!/bin/bash

# wide 200:100

echo "wide-style"
for f in `find . -name "bg-*-wide.jpg"`
do
    name="${f##*/}"
    name="${name%.*}"
    name="../src/img/${name}"
    echo $name
    convert $f -resize 992 -quality 90 $name-md.jpg
    convert $f -resize 1200 -quality 90 $name-lg.jpg
    convert $f -resize 1500 -quality 90 $name-xl.jpg
    convert $f -resize 2000 -quality 90 $name-xxl.jpg
    convert $f -resize 3000 -quality 90 $name-xxxl.jpg
    convert $f -resize 4000 -quality 90 $name-xxxxl.jpg
done

